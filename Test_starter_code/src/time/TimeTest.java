package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds=Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds ==3661);
	}
	@Test
	public void testGetTotalSecondsBin() {
		int totalSeconds=Time.getTotalSeconds("00:00:59");
		assertTrue("The time provided does not match the result", totalSeconds ==59);
	}
  @Test(expected=NumberFormatException.class)
	public void testGetTotalSecondsBout() {
		int totalSeconds=Time.getTotalSeconds("00:00:60");
		fail("The time provided is invalid");
	}
   @Test(expected=NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds=Time.getTotalSeconds("01:01:0A");
		fail("The time provided is not valid");
	}
   
   
   
   
   
  @Test  	
   public void testGetMillisecondsRegular() { 		
	   int totalMilliseconds=Time.getMilliSeconds("12:05:50:07"); 		
	   assertTrue("Invalid number of milliseconds",totalMilliseconds==7); 
	   } 
  
   @Test (expected=NumberFormatException.class) 	
   public void testGetMillisecondsException() {
		int totalMilliseconds=Time.getMilliSeconds("12:05:50:5A");
		fail("The time provided is not valid");
		
	}
   
  
   @Test  	
   public void testGetMillisecondsBoundaryIn() { 		
	   int totalMilliseconds=Time.getMilliSeconds("12:05:50:999"); 		
	   assertTrue("Invalid number of milliseconds",totalMilliseconds==999); 
	   } 	
   
   
   @Test (expected=NumberFormatException.class) 	
   public void testGetMillisecondsBoundaryOut() {
		int totalMilliseconds=Time.getMilliSeconds("12:05:50:1000");
		fail("The time provided is not valid");
		
	}

}
